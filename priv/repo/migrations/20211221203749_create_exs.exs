defmodule Discuss.Repo.Migrations.CreateExs do
  use Ecto.Migration

  def change do
    create table(:exs) do
      add :title, :string

      timestamps()
    end
  end
end
