defmodule Discuss.ExampleTest do
  use Discuss.DataCase

  alias Discuss.Example

  describe "exs" do
    alias Discuss.Example.Ex

    import Discuss.ExampleFixtures

    @invalid_attrs %{title: nil}

    test "list_exs/0 returns all exs" do
      ex = ex_fixture()
      assert Example.list_exs() == [ex]
    end

    test "get_ex!/1 returns the ex with given id" do
      ex = ex_fixture()
      assert Example.get_ex!(ex.id) == ex
    end

    test "create_ex/1 with valid data creates a ex" do
      valid_attrs = %{title: "some title"}

      assert {:ok, %Ex{} = ex} = Example.create_ex(valid_attrs)
      assert ex.title == "some title"
    end

    test "create_ex/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Example.create_ex(@invalid_attrs)
    end

    test "update_ex/2 with valid data updates the ex" do
      ex = ex_fixture()
      update_attrs = %{title: "some updated title"}

      assert {:ok, %Ex{} = ex} = Example.update_ex(ex, update_attrs)
      assert ex.title == "some updated title"
    end

    test "update_ex/2 with invalid data returns error changeset" do
      ex = ex_fixture()
      assert {:error, %Ecto.Changeset{}} = Example.update_ex(ex, @invalid_attrs)
      assert ex == Example.get_ex!(ex.id)
    end

    test "delete_ex/1 deletes the ex" do
      ex = ex_fixture()
      assert {:ok, %Ex{}} = Example.delete_ex(ex)
      assert_raise Ecto.NoResultsError, fn -> Example.get_ex!(ex.id) end
    end

    test "change_ex/1 returns a ex changeset" do
      ex = ex_fixture()
      assert %Ecto.Changeset{} = Example.change_ex(ex)
    end
  end
end
