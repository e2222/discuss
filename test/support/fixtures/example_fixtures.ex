defmodule Discuss.ExampleFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Discuss.Example` context.
  """

  @doc """
  Generate a ex.
  """
  def ex_fixture(attrs \\ %{}) do
    {:ok, ex} =
      attrs
      |> Enum.into(%{
        title: "some title"
      })
      |> Discuss.Example.create_ex()

    ex
  end
end
