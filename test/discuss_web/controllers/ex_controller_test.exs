defmodule DiscussWeb.ExControllerTest do
  use DiscussWeb.ConnCase

  import Discuss.ExampleFixtures

  @create_attrs %{title: "some title"}
  @update_attrs %{title: "some updated title"}
  @invalid_attrs %{title: nil}

  describe "index" do
    test "lists all exs", %{conn: conn} do
      conn = get(conn, Routes.ex_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Exs"
    end
  end

  describe "new ex" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.ex_path(conn, :new))
      assert html_response(conn, 200) =~ "New Ex"
    end
  end

  describe "create ex" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.ex_path(conn, :create), ex: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.ex_path(conn, :show, id)

      conn = get(conn, Routes.ex_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Ex"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.ex_path(conn, :create), ex: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Ex"
    end
  end

  describe "edit ex" do
    setup [:create_ex]

    test "renders form for editing chosen ex", %{conn: conn, ex: ex} do
      conn = get(conn, Routes.ex_path(conn, :edit, ex))
      assert html_response(conn, 200) =~ "Edit Ex"
    end
  end

  describe "update ex" do
    setup [:create_ex]

    test "redirects when data is valid", %{conn: conn, ex: ex} do
      conn = put(conn, Routes.ex_path(conn, :update, ex), ex: @update_attrs)
      assert redirected_to(conn) == Routes.ex_path(conn, :show, ex)

      conn = get(conn, Routes.ex_path(conn, :show, ex))
      assert html_response(conn, 200) =~ "some updated title"
    end

    test "renders errors when data is invalid", %{conn: conn, ex: ex} do
      conn = put(conn, Routes.ex_path(conn, :update, ex), ex: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Ex"
    end
  end

  describe "delete ex" do
    setup [:create_ex]

    test "deletes chosen ex", %{conn: conn, ex: ex} do
      conn = delete(conn, Routes.ex_path(conn, :delete, ex))
      assert redirected_to(conn) == Routes.ex_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.ex_path(conn, :show, ex))
      end
    end
  end

  defp create_ex(_) do
    ex = ex_fixture()
    %{ex: ex}
  end
end
