defmodule DiscussWeb.ExController do
  use DiscussWeb, :controller

  alias Discuss.Example
  alias Discuss.Example.Ex

  def index(conn, _params) do
    exs = Example.list_exs()
    render(conn, "index.html", exs: exs)
  end

  def new(conn, _params) do
    changeset = Example.change_ex(%Ex{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"ex" => ex_params}) do
    case Example.create_ex(ex_params) do
      {:ok, ex} ->
        conn
        |> put_flash(:info, "Ex created successfully.")
        |> redirect(to: Routes.ex_path(conn, :show, ex))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    ex = Example.get_ex!(id)
    render(conn, "show.html", ex: ex)
  end

  def edit(conn, %{"id" => id}) do
    ex = Example.get_ex!(id)
    changeset = Example.change_ex(ex)
    render(conn, "edit.html", ex: ex, changeset: changeset)
  end

  def update(conn, %{"id" => id, "ex" => ex_params}) do
    ex = Example.get_ex!(id)

    case Example.update_ex(ex, ex_params) do
      {:ok, ex} ->
        conn
        |> put_flash(:info, "Ex updated successfully.")
        |> redirect(to: Routes.ex_path(conn, :show, ex))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", ex: ex, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    ex = Example.get_ex!(id)
    {:ok, _ex} = Example.delete_ex(ex)

    conn
    |> put_flash(:info, "Ex deleted successfully.")
    |> redirect(to: Routes.ex_path(conn, :index))
  end
end
