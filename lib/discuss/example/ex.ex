defmodule Discuss.Example.Ex do
  use Ecto.Schema
  import Ecto.Changeset

  schema "exs" do
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(ex, attrs) do
    ex
    |> cast(attrs, [:title])
    |> validate_required([:title])
  end
end
