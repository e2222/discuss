defmodule Discuss.Example do
  @moduledoc """
  The Example context.
  """

  import Ecto.Query, warn: false
  alias Discuss.Repo

  alias Discuss.Example.Ex

  @doc """
  Returns the list of exs.

  ## Examples

      iex> list_exs()
      [%Ex{}, ...]

  """
  def list_exs do
    Repo.all(Ex)
  end

  @doc """
  Gets a single ex.

  Raises `Ecto.NoResultsError` if the Ex does not exist.

  ## Examples

      iex> get_ex!(123)
      %Ex{}

      iex> get_ex!(456)
      ** (Ecto.NoResultsError)

  """
  def get_ex!(id), do: Repo.get!(Ex, id)

  @doc """
  Creates a ex.

  ## Examples

      iex> create_ex(%{field: value})
      {:ok, %Ex{}}

      iex> create_ex(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_ex(attrs \\ %{}) do
    %Ex{}
    |> Ex.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a ex.

  ## Examples

      iex> update_ex(ex, %{field: new_value})
      {:ok, %Ex{}}

      iex> update_ex(ex, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_ex(%Ex{} = ex, attrs) do
    ex
    |> Ex.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a ex.

  ## Examples

      iex> delete_ex(ex)
      {:ok, %Ex{}}

      iex> delete_ex(ex)
      {:error, %Ecto.Changeset{}}

  """
  def delete_ex(%Ex{} = ex) do
    Repo.delete(ex)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking ex changes.

  ## Examples

      iex> change_ex(ex)
      %Ecto.Changeset{data: %Ex{}}

  """
  def change_ex(%Ex{} = ex, attrs \\ %{}) do
    Ex.changeset(ex, attrs)
  end
end
